<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new animal("shaun");
echo "Name: $sheep->name <br>"; // "shaun"
echo "Legs: $sheep->legs <br>"; // 2
echo "Cold_blooded: $sheep->cold_blooded <br>"; // false

echo "<br>";

echo "(Method get_name)<br>";
$sheep->set_name("shaun");
echo "Name: ". $sheep->get_name(). "<br>";
$sheep->set_legs(2);
echo "Legs: ". $sheep->get_legs(). "<br>";
$sheep->set_legs("false");
echo "Cold_blooded: ". $sheep->get_cold_blooded(). "<br>";

echo "<br>";

$ape = new ape("ape");
echo "Name: $ape->name <br>";
echo "Legs: $ape->legs <br>";
echo "Cold_blooded: $ape->cold_blooded <br>";
echo "Sound: ";
echo $ape->yell(). "<br>";

echo "<br>";

$frog = new frog("frog");
echo "Name: $frog->name <br>";
echo "Legs: $frog->legs <br>";
echo "Cold_blooded: $frog->cold_blooded <br>";
echo "Sound: ";
echo $frog->jump();

?>